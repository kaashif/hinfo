module Parse where

import Control.Monad

type Format = [Token]
data Token = Date | Music | Volume | Temperature | Wifi | Load | Battery
    deriving (Show,Eq)

parseWord :: String -> Maybe Token
parseWord w
    | w=="date" || w=="time" = Just Date
    | w=="music" || w=="song" = Just Music
    | w=="volume" = Just Volume
    | w=="temp" = Just Temperature
    | w=="wifi" = Just Wifi
    | w=="load" = Just Load
    | w=="battery" = Just Battery
    | otherwise = Nothing

parse :: String -> Maybe Format
parse s = if elem Nothing tokens then Nothing else sequence tokens
    where tokens = map parseWord (words s)

