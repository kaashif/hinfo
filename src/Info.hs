{-# LANGUAGE OverloadedStrings #-}

module Info where

import Parse
import Data.Maybe
import Data.Either.Utils
import Network.MPD hiding (Date)
import Data.Text.Format
import Text.Printf
import Data.Time
import qualified Data.Text as T
import qualified Data.Text.IO as TI
import System.Locale
import Data.Time.LocalTime
import Data.List.Split
import Data.Time.Format
import System.Locale
import qualified Data.Text.Lazy as TL
import qualified Data.Map as M
import Data.Text.Lazy.Builder (toLazyText)
import System.Process
import System.Posix.LoadAvg
import Data.List (intersperse,elemIndex)
import Control.Arrow ((>>>))
import Icon

data DzenOpts = DzenOpts
    { fg :: T.Text
    , bg :: T.Text
    , icons :: Bool
    }

a = T.append

formatSeconds t = show (minutes t) ++ ":" ++ (TL.unpack $ toLazyText $ left 2 '0' $ show (seconds t))
    where minutes t = floor $ t/60
          seconds t = (floor t) `mod` 60

songInfo :: Song -> Status -> T.Text
songInfo song stat = T.pack $ unwords $
    [ get Artist
    , "-"
    , take 30 $ get Title
    , (formatSeconds $ fst $ fromMaybe (0,0) $ stTime stat)
      ++ "/" ++
      (formatSeconds $ toRational $ snd $ fromMaybe (0,0) $ stTime stat)
    ]
    where get tag = unwords $ map toString $ fromMaybe [""] $ M.lookup tag (sgTags song)

displayAvg :: LoadAvg -> T.Text
displayAvg avg = T.pack $ unwords $ map (printf "%.2f") [sample_1 avg,sample_5 avg,sample_15 avg]

getInfo :: Token -> IO T.Text
getInfo Music = do
    i <- info
    case i of
        Right a -> return a
        Left a -> return "Could not connect to MPD"
    where info = withMPD $ do song <- currentSong
                              stat <- status
                              case song of
                                Just so -> return $ songInfo so stat
                                Nothing -> return "No song playing" 
getInfo Load = do
    raw <- getLoadAvgSafe
    case raw of
        Just avg -> return $ displayAvg avg
        Nothing -> return "Error"

getInfo Date = do
    now <- getZonedTime
    return $ T.pack $ formatTime defaultTimeLocale "%a %d-%m-%Y %H:%M:%S" now

getInfo Volume = fmap (T.pack . filter (/='\n') . unwords . drop 1 . splitOn "=") $ readProcess "mixerctl" ["outputs.master"] []

getInfo Temperature = fmap (T.pack . filter (/='\n')) $ readProcess "sysctl" ["-n", "hw.sensors.cpu0.temp0"] []

getInfo Wifi = fmap f $ readProcess "ifconfig" [] []
                where f = lines >>>
                          map words >>>
                          filter (elem "nwid") >>> 
                          (\l -> l !! 0) >>>
                          (\l -> l !! ((+1) $ fromMaybe 1 $ elemIndex "nwid" l)) >>>
                          T.pack

getInfo Battery = fmap (T.pack.(++"%").init) $ readProcess "apm" ["-l"] []

getInfo _ = return "Undefined token: impossible"

chooseIcon :: DzenOpts -> Token -> IO T.Text
chooseIcon opts t = if icons opts then do
        ipath <- getIconPath t
        case ipath of
            Just i -> return $ " ^fg(" `a` (bg opts) `a` ")^i(" `a` (T.pack i) `a` ")"
            Nothing -> return ""
    else
        return ""

getDzenInfo :: DzenOpts -> Token -> IO T.Text
getDzenInfo opts t = do
    info <- getInfo t
    icon <- chooseIcon opts t
    return $ icon `a` " ^fg(" `a` (fg opts) `a` ")" `a` info

getAllInfo :: Maybe [Token] -> IO T.Text
getAllInfo ts = case ts of
                  Just toks -> fmap (T.unwords . intersperse "|") $ mapM getInfo toks
                  Nothing -> return "Error in format string"

getAllDzenInfo :: DzenOpts -> Maybe [Token] -> IO T.Text
getAllDzenInfo opts ts = case ts of
                           Just toks -> fmap ((flip T.append) " " . T.unwords) $ mapM (getDzenInfo opts) toks
                           Nothing -> return "Error in format string"
