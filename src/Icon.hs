module Icon where

import Data.ConfigFile
import System.Directory
import Data.Either.Utils
import Control.Exception
import Parse
import qualified Data.Map as M

rcfile :: IO String
rcfile = fmap (++"/.hinforc") getHomeDirectory

defaultConfig :: IO String
defaultConfig = do
    h <- getHomeDirectory
    return $ unlines [ "date="++h++"/.xbm/clock.xbm",
        "music="++h++"/.xbm/note.xbm",
        "volume="++h++"/.xbm/spkr_01.xbm",
        "temp="++h++"/.xbm/temp.xbm",
        "wifi="++h++"/.xbm/wifi_01.xbm",
        "load="++h++"/.xbm/cpu.xbm",
        "battery="++h++"/.xbm/bat_full_0.xbm"
        ]

ensureConfig :: IO ()
ensureConfig = do
    cfg <- defaultConfig
    rc <- rcfile
    exists <- doesFileExist rc
    if exists then return () else writeFile rc cfg

getIcon :: String -> IO FilePath
getIcon s = do
    cfg <- rcfile
    cp <- readfile emptyCP cfg
    let val = forceEither cp
    return $ forceEither $ get val "DEFAULT" s

getIconPath :: Token -> IO (Maybe FilePath)
getIconPath t = do
    icon <- try (case t of
        Date -> getIcon "date"
        Music -> getIcon "music"
        Volume -> getIcon "volume"
        Temperature -> getIcon "temp"
        Wifi -> getIcon "wifi"
        Battery -> getIcon "battery"
        Load -> getIcon "load") :: IO (Either SomeException FilePath)
    case icon of
        Left ex -> putStrLn "Couldn't get icon!" >> return Nothing
        Right path -> return $ Just path
